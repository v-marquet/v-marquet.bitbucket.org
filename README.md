BarTendr
--------
Simulation du serveur pour BarTendr. Pour le vrai serveur, il faudra remplacer `v-marquet.bitbucket.org/bartendr` par l'adresse IP du raspberry pi.

* [http://v-marquet.bitbucket.org/bartendr/menu.json](http://v-marquet.bitbucket.org/bartendr/menu.json)
* [http://v-marquet.bitbucket.org/bartendr/categories/0.json](http://v-marquet.bitbucket.org/bartendr/categories/0.json)
* [http://v-marquet.bitbucket.org/bartendr/articles/0.json](http://v-marquet.bitbucket.org/bartendr/articles/0.json)


Choose
------
* [http://v-marquet.bitbucket.org/choose/interests.json](http://v-marquet.bitbucket.org/choose/interests.json)
